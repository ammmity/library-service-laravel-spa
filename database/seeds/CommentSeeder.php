<?php

use App\Entity\Comment;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');;

        foreach (range(1, 30) as $index) {
            $user = new Comment();
            $user->text = $faker->sentence(5);
            $user->rating = $faker->numberBetween(1, 5);
            $user->book_id = $faker->numberBetween(1, 30);
            $user->user_id = $faker->numberBetween(1, 10);
            $user->save();
        }
    }
}
