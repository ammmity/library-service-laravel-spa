<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {

    Route::post('register', 'AuthController@register')->name('register');
    Route::post('login', 'AuthController@login')->name('login');
    Route::post('logout', 'AuthController@logout')->name('logout');
    Route::get('refresh', 'AuthController@refresh')->name('refresh');
    Route::post('me', 'AuthController@me')->name('me');

});



Route::apiResource('books', 'BookController')->only([
    'index', 'show'
]);
Route::middleware('api')->post('books/search', 'BookController@search')->name('books.search');
Route::middleware('api')->get('books/{book}/comments', 'BookController@comments')->name('books.get_comments');



// Route::apiResource('comments', 'CommentController')->only([
//     'index'
// ]);



Route::group([
    'middleware' => ['api', 'auth:api', 'customer'],
    'prefix' => 'customer',
], function () {

    Route::get('/reserved_books', 'CustomerController@reservedBooks')->name('customer.reserved_books');
    Route::get('/reserved_books/search', 'CustomerController@searchReservedBooks')->name('customer.search_reserved_books');
    Route::post('/reserve_book', 'CustomerController@reserveBook')->name('customer.reserve_book');
    Route::post('/withdraw_reserve', 'CustomerController@withdrawReserve')->name('customer.withdraw_reserved_books');
    Route::post('/add_comment', 'CustomerController@addComment')->name('customer.add_comment');

});



Route::group(['middleware' => ['api', 'auth:api', 'librarian'], 'prefix' => 'librarian',], function () {

    Route::get('/users/{user}', 'UserController@show')->name('librarian.users.show');
    Route::post('/books/store', 'BookController@store')->name('librarian.books.store');
    Route::post('/books/destroy', 'BookController@destroy')->name('librarian.books.destroy');
    Route::post('/books/issue', 'BookReserveController@issueReservedBook')->name('librarian.books.issue_reserved_book');
    Route::post('/books/take', 'BookReserveController@takeIssuedBook')->name('librarian.books.take_issued_book');

});



Route::group(['middleware' => ['api', 'auth:api', 'role:admin']], function () {

    Route::apiResource('users', 'UserController');
    Route::post('users/search', 'UserController@search')->name('users.search');
    Route::post('users/change_password', 'UserController@changePassword')->name('users.change_password');

    Route::get('roles', 'RoleController@index')->name('roles.index');

});
