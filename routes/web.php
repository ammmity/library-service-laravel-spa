<?php

use Illuminate\Support\Facades\Route;

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/{any}', function () {
    return view('home');
})->where('any', '.*');
