require('./bootstrap');

//window.Vue = require('vue');
import Vue from 'vue';
import App from './components/App'
import VueRouter from 'vue-router';
import routes from './routes';
import store from './store';
import Notifications from 'vue-notification';

Vue.use(VueRouter);
Vue.use(Notifications);


export const router = new VueRouter(routes);
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {

        if (!store.state.user.isLoggedIn) {
            next({name: 'Login'});
        } else {
            if (store.state.user.info.role_id !== to.meta.roleId) {
                next({name: 'LibraryBooks'});
            } else {
                next();
            }
        }

    } else {
        next();
    }
});

let app = new Vue({
    el: '#app-root',
    store: store,
    router: router,
    render: h => h(App),
    created: function () {
        if (this.$store.state.user.token !== '' && !this.$store.state.user.isLoggedIn) {
            this.$store.dispatch('authRefresh', { token: this.$store.state.user.token });
        }
    },
});
