import NotFound from './components/pages/NotFoundPage';
import LibraryBooks from './components/pages/LibraryBooksPage';
import LibraryBook from './components/pages/LibraryBookPage';
import Login from './components/pages/LoginPage';
import Register from './components/pages/RegisterPage';
import ReservedBooks from './components/pages/ReservedBooksPage';
import Books from './components/pages/BooksPage';
import Users from './components/pages/UsersPage';

export default {
    mode: 'history',
    routes: [
        {
            path: '*',
            component: NotFound,
            name: 'NotFound',
        },
        {
            path: '/',
            component: LibraryBooks,
            name: 'LibraryBooks'
        },
        {
            path: '/login',
            component: Login,
            name: 'Login'
        },
        {
            path: '/register',
            component: Register,
            name: 'Register'
        },
        {
            path: '/book/:id',
            component: LibraryBook,
            name: 'LibraryBook'
        },
        {
            path: '/customer/reserved_books',
            component: ReservedBooks,
            name: 'ReservedBooks',
            meta: {
                requiresAuth: true,
                roleId: 3
            }
        },
        {
            path: '/librarian/books',
            component: Books,
            name: 'Books',
            meta: {
                requiresAuth: true,
                roleId: 2
            }
        },
        {
            path: '/admin/users',
            component: Users,
            name: 'Users',
            meta: {
                requiresAuth: true,
                roleId: 1
            }
        },
    ]
};


