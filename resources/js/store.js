import Vue from 'vue';
import Vuex from 'vuex';
import {router} from './app.js'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: {
            info: {},
            token: window.localStorage.getItem('token') || '',
            isLoggedIn: false
        },
    },
    mutations: {
        loginUser: function (state, payload) {
            state.user.info = payload.data.user;
            localStorage.setItem('token', payload.data.access_token);
            state.user.token = payload.data.access_token;
            state.user.isLoggedIn = true;
            if (router.currentRoute.name != 'LibraryBooks') {
                router.push({name: 'LibraryBooks'});
            }
        },
        logoutUser: function (state) {
            state.user.info = {};
            state.user.token = '';
            state.user.isLoggedIn = false;
            localStorage.setItem('token', '');
            if (router.currentRoute.name != 'Login') {
                router.push({name: 'Login'});
            }
        },
    },
    actions: {
        authRefresh: async (context, payload) => {
            let token, user, responseWithToken, responseWithUser;

            try {
                responseWithToken = await axios.get('/api/auth/refresh?token=' + payload.token);
                token = responseWithToken.data.access_token;
            } catch (er) {
                context.commit('logoutUser');
                // todo: show errors to client
                return;
            }

            try {
                responseWithUser = await axios.post('/api/auth/me?token=' + token);
                user = responseWithUser.data;
            } catch (er) {
                context.commit('logoutUser');
                // todo: show errors to client
                return;
            }

            context.commit('loginUser', {
                data: {
                    user: user,
                    access_token: token
                }
            });
        },
    }
});
