<?php

namespace App\Http\Middleware;

use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        $userRole = $request->user()->role;
        foreach($roles as $role) {
            if ($userRole->slug === $role) {
                return $next($request);
            }
        }
        return response()->json([
            'success' => false,
            'errors' => [
                'message' => ["{$userRole->name} - не имеет доступа к этому ресурсу."]
            ]
        ], 403);
    }
}
