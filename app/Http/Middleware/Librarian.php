<?php

namespace App\Http\Middleware;

use Closure;

class Librarian
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->role_id !== 2) {
            return response()->json([
                'success' => false,
                'errors' => [
                    'message' => ['Доступно только для библиотекарей.']
                ]
            ], 403);
        }
        return $next($request);
    }
}
