<?php

namespace App\Http\Resources;

use App\Entity\BooksReserve;
use App\Http\Resources\BooksReserve as BooksReserveResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Book extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'author' => $this->author,
            'genre' => $this->genre,
            'comments' =>  $this->whenLoaded('Comments'),
            'books_reserve' => $this->whenLoaded('BooksReserve'),
            'publisher' => $this->publisher,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
       // return parent::toArray($request);
    }
}
