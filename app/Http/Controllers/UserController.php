<?php

namespace App\Http\Controllers;

use App\Entity\Book;
use App\Entity\User;
use App\Entity\BooksReserve;
use App\Http\Requests\UserChangePasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\BookCollection;
use App\Http\Resources\UserCollection;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserSearchRequest;
use App\Http\Requests\UserDestroyRequest;
use App\Http\Resources\User as UserResource;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new UserCollection(User::with('Role')->paginate(5));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $user = User::create($request->validated());
        return response()->json([
            'success' => true,
            'message' => 'Книга успешно создана',
            'data' => $user
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new UserResource(User::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (User::where('id', $id)->first() === null) {
            return response()->json([
                'success' => false,
                'message' => 'Пользователь не найден.',
            ]);
        }

        $userBooksReserve = BooksReserve::where('user_id', $id)->get(['id', 'issued']);

        if (!count($userBooksReserve)) {

            User::destroy($id);
            return response()->json([
                'success' => true,
                'message' => 'Пользователь удалён.',
            ]);

        } else {
            $reservedBooksId = [];
            $haveIssuedBooks = false;

            foreach ($userBooksReserve as $reserve) {
                if ($reserve->issued) {
                    $haveIssuedBooks = true;
                    break;
                }
                $reservedBooksId[] = $reserve->id;
            }

            if ($haveIssuedBooks) {
                return response()->json([
                    'success' => false,
                    'message' => 'Пользователь не может быть удалён, т.к имеет выданные книги.',
                ]);
            } else {
                BooksReserve::destroy($reservedBooksId);
                User::destroy($id);

                return response()->json([
                    'success' => true,
                    'message' => 'Пользователь удалён.',
                ]);
            }
        }
    }

    public function search(UserSearchRequest $request)
    {
        return new UserCollection(
            User::with('Role')
                ->where('name', 'like', "%{$request->search}%")
                ->orWhere('email', 'like', "%{$request->search}%")
                ->paginate(5)
        );
    }

    public function changePassword(UserChangePasswordRequest $request)
    {
        $user = User::find($request->id);
        $user->password = Hash::make($request->password);
        $user->save();
        return response()->json([
            'success' => true,
            'message' => 'Пароль успешно изменён.'
        ]);
    }
}
