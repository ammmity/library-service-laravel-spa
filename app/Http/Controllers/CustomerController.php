<?php

namespace App\Http\Controllers;

use App\Entity\Book;
use App\Entity\BooksReserve;
use App\Entity\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\BookAddComment;
use App\Http\Resources\BookCollection;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Resources\Book as BookResource;
use App\Http\Requests\BookReservationRequest;
use App\Http\Requests\CustomerWithdrawReserveRequest;

class CustomerController extends Controller
{
    public function reserveBook(BookReservationRequest $request)
    {
        $book = Book::with(['BooksReserve'])->find($request->book_id);
        $isAvailableToReserve = $book !== null && $book->booksReserve === null;
        if ($isAvailableToReserve) {
            $booksReserve = new BooksReserve();
            $booksReserve->user_id = $request->user()->id;
            $booksReserve->issued = false;
            $booksReserve->reserved_from = Carbon::today()->format('Y-m-d');
            $booksReserve->reserved_to = Carbon::tomorrow()->format('Y-m-d');
            $book->booksReserve()->save($booksReserve);
            return response()->json([
                'success' => true,
                'message' => 'Книга успешно зарезервированна',
                'booksReserve' => $booksReserve
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Книгу нельзя зарезервировать, т.к она уже зарезервированна другим пользователем'
            ], 422);
        }
    }


    public function reservedBooks(Request $request)
    {
        return new BookCollection(
            Book::with('BooksReserve')
                ->whereHas('booksReserve', function (Builder $q) use ($request) {
                    return $q->where([
                        ['user_id', '=', $request->user()->id],
                        ['issued', false]
                    ]);
                })->paginate(5)
        );
    }


    public function withdrawReserve(CustomerWithdrawReserveRequest $request)
    {
        $booksReserve = BooksReserve::where([
            ['user_id', $request->user()->id],
            ['book_id', $request->book_id]
        ])->first();

        if ($booksReserve) {
            $booksReserve->delete();
            return response()->json([
                'success' => true,
                'message' => 'Книга снята с резерва'
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Ошибка снятия резерва'
        ]);
    }


    public function addComment(BookAddComment $request)
    {
        $comment = new Comment();
        $comment->text = $request->text;
        $comment->rating = $request->rating;
        $comment->book_id = $request->book_id;
        $comment->user_id = Auth::user()->id;
        $comment->save();
        $comment->user = [
            'id' => Auth::user()->id,
            'name' => Auth::user()->name,
            'surname' => Auth::user()->surname,
        ];
        return response()->json([
            'success' => true,
            'message' => 'Добавлен новый отзыв.',
            'data' => $comment
        ]);
    }

}
