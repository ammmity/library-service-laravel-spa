<?php

namespace App\Http\Controllers;

use App\Entity\Book;
use App\Entity\BooksReserve;
use Illuminate\Http\Request;
use App\Http\Requests\BookIssueRequest;
use App\Http\Resources\BooksReserve as BooksReserveResource;

class BookReserveController extends Controller
{
    public function issueReservedBook(BookIssueRequest $request)
    {
        $bookReserve = BooksReserve::where([
            ['user_id', $request->user_id],
            ['book_id', $request->book_id]
        ])->first();

        if ($bookReserve && !$bookReserve->issued) {
            $bookReserve->issued = true;
            $bookReserve->save();
            return response()->json([
                'success' => true,
                'message' => 'Книга успешно выдана пользователю',
                'data' => new BooksReserveResource($bookReserve)
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Книга не резервировалась пользователем, либо уже выдана.',
        ]);
    }

    public function takeIssuedBook(Request $request)
    {
        $bookReserve = BooksReserve::query()->where([
            ['user_id', $request->user_id],
            ['book_id', $request->book_id],
            ['issued', true]
        ])->first();

        if ($bookReserve) {
            $bookReserve->delete();
            return response()->json([
                'success' => true,
                'message' => 'Резерв успешно снят.',
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Резерв найден, но книга не выдавалсь пользователю.',
        ]);
    }
}
