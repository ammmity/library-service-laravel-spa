<?php

namespace App\Http\Controllers;

use App\Entity\Role;
use App\Http\Resources\RoleCollection;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new RoleCollection(Role::all());
    }
}
