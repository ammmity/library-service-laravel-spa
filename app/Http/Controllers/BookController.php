<?php

namespace App\Http\Controllers;

use App\Entity\Book;
use App\Entity\Comment;
use App\Entity\BooksReserve;
use Illuminate\Http\Request;
use App\Http\Resources\BookCollection;
use App\Http\Requests\BookIssueRequest;
use App\Http\Requests\BookStoreRequest;
use App\Http\Requests\BookSearchRequest;
use App\Http\Requests\BookDestroyRequest;
use App\Http\Resources\CommentCollection;
use App\Http\Resources\Book as BookResource;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new BookCollection(Book::with('BooksReserve')->paginate(5));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookStoreRequest $request)
    {
        $book = Book::create($request->validated());
        return response()->json([
            'success' => true,
            'message' => 'Книга успешно создана',
            'data' => $book
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::with([
            'Comments.user' => function ($query) {
                $query->select('id', 'name', 'surname');
            },
            'BooksReserve'
        ])->find($id);
        if ($book === null) {
            return response()->json([
                'success' => false,
                'message' => 'Книга не найден.',
            ]);
        }
        return response()->json(new BookResource($book));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookDestroyRequest $request)
    {
        $book = Book::with('BooksReserve')->findOrFail($request->id);
        if ($book->booksReserve === null) {
            $book->comments()->delete();
            $book->delete();
            return response()->json([
                'success' => true,
                'message' => 'Книга успешно удалена',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Нельзя удалить зарезервированную книгу',
            ]);
        }

    }

    public function search(BookSearchRequest $request)
    {
        return new BookCollection(
            Book::with('BooksReserve')
                ->where('name', 'like', "%{$request->search}%")
                ->orWhere('author', 'like', "%{$request->search}%")
                ->orWhere('genre', 'like', $request->search)
                ->orWhere('publisher', 'like', $request->search)
                ->paginate(5)
        );
    }

    // public function comments($id) {
    //     $book = Book::with('Comments')->where('id', $id)->first();

    //     if ($book === null) {
    //         return response()->json([
    //             'success' => false,
    //             'messsage' => 'Книга не найдена.'
    //         ]);
    //     }

    //     return new CommentCollection($book->comments);
    // }
}
