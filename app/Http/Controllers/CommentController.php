<?php

namespace App\Http\Controllers;

use App\Entity\Comment;
use App\Http\Resources\CommentCollection;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new CommentCollection(Comment::paginate(5));
    }
}
