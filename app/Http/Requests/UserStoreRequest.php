<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check() && $this->user()->role_id === 1) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|unique:users|email',
            'role_id' => 'required|integer',
            'password' => 'required|min:6',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Имя - обязательное поле',
            'surname.required' => 'Фамилия - обязательное поле',
            'email.required' => 'Почта - обязательное поле',
            'email.unique' => 'Пользователь с такой почтой уже зарегистрирован',
            'email.email' => 'Почта - должна иметь формат почты',
            'role_id.required' => 'Роль - обязательное поле',
            'role_id.integer' => 'Роль - должно быть целочисленным значением',
            'password.required' => 'Пароль - обязательное поле',
            'password.min' => 'Пароль - минимальное число символов должно быть не меньше 6',
        ];
    }
}
