<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class UserChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check() && $this->user()->role_id === 1) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:users',
            'password' => 'required|min:8'
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'ID - обязательное поле',
            'id.exists' => 'ID - Выбран несуществующий пользователь.',
            'password.required' => 'Пароль - обязательное поле',
            'password.min' => 'Пароль - минимальное число символов 8',
        ];
    }
}
