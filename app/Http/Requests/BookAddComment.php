<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class BookAddComment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check() && $this->user()->role_id === 3) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'book_id' => 'required|exists:books,id|integer',
            'text' => 'required',
            'rating' => 'required|integer|min:1|max:6'
        ];
    }

    public function messages()
    {
        return [
            'book_id.required' => 'ID книги - обязательное поле',
            'book_id.exists' => 'Не найдена книга с данным ID',
            'book_id.integer' => 'ID книги - должно быть числом',
            'text.required' => 'Текст комментария - обязательное поле',
            'rating.required' => 'Оценка - Обязательное поле',
            'rating.integer' => 'Оценка - должно быть числом',
            'rating.min' => 'Рейтинг - минимальное значение 1',
            'rating.max' => 'Рейтинг - максимальное значение 5',
        ];
    }
}
