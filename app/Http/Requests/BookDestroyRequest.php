<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class BookDestroyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check() && $this->user()->role_id === 2) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:books,id|integer'
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'ID книги - обязательное поле',
            'id.exists' => 'Не найдена книга с данным ID',
            'id.integer' => 'ID книги - должно быть числом',
            'id.exists' => 'Выбрана несуществующая книга',
        ];
    }
}
