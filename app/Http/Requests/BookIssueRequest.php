<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class BookIssueRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check() && $this->user()->role_id === 2) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'book_id' => 'required|integer',
            'user_id' => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'book_id.required' => 'ID книги - обязательное поле',
            'book_id.integer' => 'ID книги - должно быть числом',
            'user_id.required' => 'ID пользователя - обязательное поле',
            'user_id.integer' => 'ID пользователя - должно быть числом',
        ];
    }
}
