<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Имя - обязательное поле',
            'name.max' => 'Имя - максимальное число символом 255',
            'surname.required' => 'Фамилия - обязательное поле',
            'surname.max' => 'Фамилия - максимальное число символом 255',
            'email.required' => 'Почта - обязательное поле',
            'email.unique' => 'Почта - пользователь с такой почтой уже зарегистрирован',
            'email.email' => 'Почта - некорректный вид почты',
            'email.max' => 'Почта - максимальное число символом 255',
            'password.required' => 'Пароль - обязательное поле',
            'password.confirmed' => 'Пароль - подтверждение пароля не совпадает с паролем',
            'password.min' => 'Пароль - минимальное число символом 8',
        ];
    }
}
