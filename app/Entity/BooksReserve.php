<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class BooksReserve extends Model
{
    protected $table = 'books_reserve';

    protected $fillable = [
        'book_id', 'user_id', 'issued', 'reserved_from', 'reserved_to'
    ];
}
